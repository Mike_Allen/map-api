import React from 'react'
import {Map, Marker} from 'react-map-gl'

function MapContainer() {

    const [viewState, setViewState] = React.useState({
        longitude: -100,
        latitude: 40,
        zoom: 3.5
    });

    const [dataState, setDataState] = React.useState(null)

    const [markerState, setMarkerState] = React.useState({
        lng: 0,
        lat: 0
    })

    const locationName = () => {
        if (dataState !== null) {

            let name = dataState.name ? dataState.name + " is currently " : "This location is currently "
            let temp = dataState.main.temp ? dataState.main.temp + " degrees celsius" : " of an unknown temperature"

            if (dataState) {
                return (
                    <p>
                        {name} {temp}
                    </p>
                )
            } else {
                return (
                    <p>
                        Data not available for this location
                    </p>
                )
            }
        }
    }

    return (
        <div>
            <Map
                mapboxAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
                {...viewState}
                onMove={evt => setViewState(evt.viewState)}
                onClick={evt => {
                    setMarkerState(evt.lngLat)
                    fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${evt.lngLat.lat}&lon=${evt.lngLat.lng}&units=metric&appid=${process.env.REACT_APP_WEATHER_API_KEY}`)
                        .then(response => response.json())
                        .then(json => setDataState(json))
                        .catch(error => setDataState(null));
                }}
                style={{width: "100vw", height: "80vh"}}
                mapStyle="mapbox://styles/mapbox/streets-v9"
            >
                {
                    (markerState.lng !== 0 && markerState.lat !== 0) &&
                    <Marker longitude={markerState.lng} latitude={markerState.lat}/>

                }
            </Map>
            <div onClick={() => {return console.log(dataState)}}>
                {locationName()}
            </div>
        </div>



    )
}

export default MapContainer