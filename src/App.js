import './App.css';
import MapContainer from "./components/map/Map.tsx";


function App() {
    return (
        <MapContainer/>
    )
}

export default App;
